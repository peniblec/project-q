OUTDIR = public

pages_src = $(filter-out template.html,$(wildcard *.html))
pages = $(foreach p,$(pages_src),$(OUTDIR)/$(p))

sheets = $(wildcard *.css)
images = $(wildcard *.svg)
feeds = $(wildcard *.xml)
resources_src = $(sheets) $(images) $(feeds)
resources = $(foreach f,$(resources_src),$(OUTDIR)/$(f))


.PHONY: all clean site

all: site

site: $(pages) $(resources)

$(OUTDIR):
	mkdir $@

$(resources): $(OUTDIR)/%: % | $(OUTDIR)
	cp $< $@

# TODO: optional dependency to .sh template parameters.
$(pages): $(OUTDIR)/%.html: %.html template.html | $(OUTDIR)
	./build.sh $< $@

$(OUTDIR)/quatuor.html: quatuor.md

clean:
	-rm -r $(OUTDIR)
