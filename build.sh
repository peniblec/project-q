#!/bin/bash

set -eu

input=$1
output=$2

parameters=${input/%.html/.sh}

if ! test -f ${parameters}
then
    cp ${input} ${output}
    exit
fi

cp template.html ${output}

transform-li-current-a ()
{
    local old="<li><a href=\"$1.html\">"
    local new="<li class=\"current\"><a href=\"$1.html\">"
    echo "s/${old}/${new}/"
}

transform-li-dropdown-current-a ()
{
    local old="<li class=\"dropdown\"><a href=\"$1.html\">"
    local new="<li class=\"dropdown current\"><a href=\"$1.html\">"
    echo "s/${old}/${new}/"
}

transforms=()
postprocess=true
. ${parameters}

sed -i s/'{TITLE}'/"${title}"/ ${output}

link_stylesheets=''
for s in "${stylesheets[@]}"
do
    link_stylesheets+=$(printf '<link rel="stylesheet" href="%s.css">\n' "${s}")
done

sed -i /'{STYLESHEETS}'/'c\'"${link_stylesheets}" ${output}

for transform in "${transforms[@]}"
do
    sed -i "${transform}" ${output}
done

sed -i -e /'{MAIN}'/"r ${input}" -e /'{MAIN}'/'c\' ${output}

${postprocess} ${output}
