add-bio ()
{
    sed -i                                      \
        -e /'{BIO}'/'r '<(pandoc quatuor.md)    \
        -e /'{BIO}'/'c\'                        \
        $1
}

title=Biographie
stylesheets=quatuor
transforms=("$(transform-li-dropdown-current-a quatuor)")
postprocess=add-bio
